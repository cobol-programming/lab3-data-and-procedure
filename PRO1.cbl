       IDENTIFICATION DIVISION. 
       PROGRAM-ID. PRO1.
       AUTHOR. KANTIDA.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  NUM1         PIC 99.
       01  NUM2         PIC 99.
       01  NUM3         PIC 99.
       01  NUM4         PIC 99.
       01  PROBLAM-STR  PIC X(50).

       PROCEDURE DIVISION .
       BEGIN.
           PERFORM PROBLAM1.
           PERFORM PROBLAM2.
           PERFORM PROBLAM3.
           PERFORM PROBLAM4.
           PERFORM PROBLAM5.
           PERFORM PROBLAM6.
           PERFORM PROBLAM7.
           PERFORM PROBLAM8.
           PERFORM PROBLAM9.
           PERFORM PROBLAM10.
           PERFORM PROBLAM11.
           PERFORM PROBLAM12.
           GOBACK.

       PROBLAM1.
           MOVE "PROBLEM1 : ADD NUM1 TO NUM2" TO PROBLAM-STR
           PERFORM HEARDER
           MOVE 25 TO NUM1
           MOVE 30 TO NUM2
           MOVE ZEROS TO NUM3
           MOVE ZEROS TO NUM4
           PERFORM DISPLAY-BEFORE
           ADD NUM1 TO NUM2
           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLAM2.
           MOVE "PROBLEM2 : ADD NUM1 , NUM2 TO NUM3 , NUM4"
              TO PROBLAM-STR.
              *> num3 = (num1 + num2) + num3
              *> num4 = (num1 + num2) + num4
           PERFORM HEARDER.
           MOVE 13 TO NUM1.
           MOVE 04 TO NUM2.
           MOVE 05 TO NUM3.
           MOVE 12 TO NUM4.
           PERFORM DISPLAY-BEFORE.
           ADD NUM1, NUM2 TO NUM3, NUM4.
           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLAM3.
           MOVE "PROBLEM3 : ADD NUM1 , NUM2 , NUM3 GIVING NUM4"
              TO PROBLAM-STR.
              *> num4 = num1 + num2 + num3
           PERFORM HEARDER.
           MOVE 04 TO NUM1.
           MOVE 03 TO NUM2.
           MOVE 02 TO NUM3.
           MOVE 01 TO NUM4.
           PERFORM DISPLAY-BEFORE.
           ADD NUM1, NUM2, NUM3 GIVING NUM4.
           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLAM4.
           MOVE "PROBLEM4 : SUBTRACT NUM1 FROM NUM2 GIVING NUM3"
              TO PROBLAM-STR.
              *> num3 = num1 - num2 
           PERFORM HEARDER.
           MOVE 04 TO NUM1.
           MOVE 10 TO NUM2.
           MOVE 55 TO NUM3.
           MOVE ZEROS TO NUM4.
           PERFORM DISPLAY-BEFORE.
           SUBTRACT NUM1 FROM NUM2 GIVING NUM3.
           PERFORM DISPLAY-AFTER 
           EXIT.    

           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLAM5.
           MOVE "PROBLEM5 : SUBTRACT NUM1 FROM NUM2 GIVING NUM3"
              TO PROBLAM-STR.
              *> num3 -= num1 
              *> num3 -= num2 
           PERFORM HEARDER.
           MOVE 05 TO NUM1.
           MOVE 10 TO NUM2.
           MOVE 55 TO NUM3.
           MOVE ZEROS TO NUM4.
           PERFORM DISPLAY-BEFORE.
           SUBTRACT NUM1, NUM2 FROM NUM3.
           PERFORM DISPLAY-AFTER 
           EXIT. 
       
       PROBLAM6.
           MOVE "PROBLEM6 : SUBTRACT NUM1 , NUM2 FROM NUM3 GIVING NUM4"
              TO PROBLAM-STR.
              *> num4 = (num1 - num2) - num 3
           PERFORM HEARDER.
           MOVE 05 TO NUM1.
           MOVE 10 TO NUM2.
           MOVE 55 TO NUM3.
           MOVE 20 TO NUM4.
           PERFORM DISPLAY-BEFORE.
           SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4.
           PERFORM DISPLAY-AFTER 
           EXIT. 

       PROBLAM7.
           MOVE "PROBLEM7 : MULTIPLY NUM1 BY NUM2"
              TO PROBLAM-STR.
              *> num2 = num1 * num2
           PERFORM HEARDER.
           MOVE 10 TO NUM1.
           MOVE 05 TO NUM2.
           MOVE ZEROS TO NUM3.
           MOVE ZEROS TO NUM4.
           PERFORM DISPLAY-BEFORE.
           MULTIPLY NUM1 BY NUM2.
           PERFORM DISPLAY-AFTER 
           EXIT. 

       PROBLAM8.
           MOVE "PROBLEM8 : MULTIPLY NUM1 BY NUM2 GIVING NUM3"
              TO PROBLAM-STR.
              *> num3 = num1 * num2
           PERFORM HEARDER.
           MOVE 10 TO NUM1.
           MOVE 05 TO NUM2.
           MOVE 33 TO NUM3.
           MOVE ZEROS TO NUM4.
           PERFORM DISPLAY-BEFORE.
           MULTIPLY NUM1 BY NUM2 GIVING NUM3.
           PERFORM DISPLAY-AFTER 
           EXIT. 

       PROBLAM9.
           MOVE "PROBLEM9 : DIVIDE NUM1 INTO NUM2"
              TO PROBLAM-STR.
              *> num2 = num2 / num1
           PERFORM HEARDER.
           MOVE 05 TO NUM1.
           MOVE 64 TO NUM2.
           MOVE ZEROS TO NUM3.
           MOVE ZEROS TO NUM4.
           PERFORM DISPLAY-BEFORE.
           DIVIDE NUM1 INTO NUM2.
           PERFORM DISPLAY-AFTER 
           EXIT. 

       PROBLAM10.
           MOVE
           "PROBLEM10 : DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER NUM4"
              TO PROBLAM-STR.
              *> num3 = num2 / num1
              *> num4 = เศษ
           PERFORM HEARDER.
           MOVE 05 TO NUM1.
           MOVE 64 TO NUM2.
           MOVE 24 TO NUM3.
           MOVE 88 TO NUM4.
           PERFORM DISPLAY-BEFORE.
           DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER NUM4.
           PERFORM DISPLAY-AFTER 
           EXIT. 

       PROBLAM11.
           MOVE
              "PROBLEM11 : COMPUTE NUM1 = 5 + 10 * 30 /2"
              TO PROBLAM-STR.
           PERFORM HEARDER.
           MOVE 25 TO NUM1.
           MOVE ZEROS TO NUM2.
           MOVE ZEROS TO NUM3.
           MOVE ZEROES TO NUM4.
           PERFORM DISPLAY-BEFORE.
           COMPUTE NUM1 = 5 + 10 * 30 / 2.
           PERFORM DISPLAY-AFTER 
           EXIT.  

       PROBLAM12.
           DISPLAY "PLEASE INPUT FIRST NUM " WITH NO ADVANCING .
           ACCEPT NUM1 .
           DISPLAY "PLEASE INPUT SECOND NUM " WITH NO ADVANCING .
           ACCEPT NUM2 .
           COMPUTE NUM3 = NUM1 + NUM2 ON SIZE ERROR DISPLAY "ERROR"
           END-COMPUTE.
           DISPLAY NUM3 .
           EXIT. 

       HEARDER.
           DISPLAY "***********************************************".
           DISPLAY PROBLAM-STR.
           DISPLAY "       NUM1  NUM2  NUM3  NUM4"
           EXIT.

       DISPLAY-BEFORE.
           DISPLAY "BEFORE  " NUM1 "    " NUM2 "    " NUM3 "    " NUM4.
           EXIT.

       DISPLAY-AFTER.
           DISPLAY "AFTER   " NUM1 "    " NUM2 "    " NUM3 "    " NUM4.
           EXIT.
